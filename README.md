# Cylindo front-end test

The goal of the test is to make a tool for annotating images.

![img](./img/example.png)

## Acceptance Criteria

- The tool should allow the user to draw boxes on an image to highlight specific areas.
- The tool should allow the user to move and delete existing boxes
- The tool should be implemented as a single page app using React
- The tool should work in all modern browsers.

## Evaluation

We will evaluate the result based on the following questions:

- Does the code do what it is supposed to?
- Is the code simple and easy to read?
- Is the code efficient?

You do not need to implement any kind of backend or any way to save the annotations. The design and UI/UX is entirely up to you.

## Delivery

The code should be delivered as a zip file containing all the relevant files.

you should be able to run the app with

```
npm install
npm build
npm start
```
